var app = angular.module('bookStore', ['ngRoute'])
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'page/home.html',
				controller: 'HomeController',
				controllerAs: 'ctrl'
			})
			.when('/book/:id', {
				templateUrl: 'page/book.html',
				controller: 'BookController',
				controllerAs: 'ctrl'
			});
	}])
	.controller('StoreController', [function() {
		this.name = 'Angular Bookstore';
	}]);