app.controller('BookController', ['$routeParams', function($routeParams) {
	this.book = books[$routeParams.id];
	this.newReview = {};

	this.addReview = function() {
		this.book.reviews.push(this.newReview);
		this.newReview = {};
	};
}]);