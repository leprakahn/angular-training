var books = [
	{
		name: 'How to Angular',
		image: 'angular.jpg',
		author: 'Sean Malone',
		date: 'Sep 8, 1999',
		description: 'This is a book about how to Angular.',
		reviews: []
	},
	{
		name: 'Harry Plopper',
		image: 'harry-plopper.jpg',
		author: 'J.K. Hogling',
		date: 'Jun 16, 2012',
		description: 'Harry is a magical wizard pig that attends Hogwarts. He constantly gets into danger with his friends.',
		reviews: []
	},
	{
		name: 'Spider Pig',
		image: 'spider-pig.jpg',
		author: 'Pork Lee',
		date: 'May 23, 1976',
		description: 'Peter Porker is a regular pig living on a farm when one day he is bitten by a radioactive spider. He becomes Spider Pig.',
		reviews: []
	}
];

var app = angular.module('bookStore', [])
	.controller('StoreController', [function() {
		this.name = 'Angular Bookstore';
		this.books = books;

		this.addReview = function(book) {
			book.reviews.push(book.newReview);
			book.newReview = {};
		};
	}]);